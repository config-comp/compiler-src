#pragma once
class LexicalScanner
{
public:
	LexicalScanner();
	~LexicalScanner();

	/*
	 * Returns the scanner instace used by the lexical analyser.
	 * Enforces singleton pattern to reduce memory footprint
	 *
	 * Return - Scanner instance used in the lexical analysis stage of 
	 *			scompilation
	 */
	LexicalScanner getInstance();

	/*
	 * Invokes the scanning process for the line being handled by the scanner.
	 * Begins the process of creating a token stream by scanning the line and 
	 * seperating into distinct lexemes.
	 */
	//TODO - Change return type to list of Tokens
	void scanLine();
};

