#pragma once
#include "Token.h"
class Evaluator
{
public:
	Evaluator();
	~Evaluator();

	/* Creates a new token object by using the map 
     *
	 returns that token object back to LexicalAnalyser
	 */
	Token assignValue();
};

