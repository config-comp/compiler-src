#pragma once
#include <iostream>

class CompilerController
{
public:
	CompilerController();
	~CompilerController();

	/*
	 * Invokes the lexical analysis stage of compilation
	 *
	 * Param sourceLine - the next line of source code that has not been passed
	 *						in to the lexical analyser
	 */
	//TODO - Change return type to list of Tokens
	void invokeLexicalAnalysis(std::string sourceLine);
};

