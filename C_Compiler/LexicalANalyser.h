#include<list>
#include "Token.h"
class LexicalAnalyser
{
public:
	LexicalAnalyser();
	~LexicalAnalyser();


	/*
	 * Calls the scanner to retreive the next word of the file.
	 * Then passes the word to the evaluator to be tokenised
	 * 
	 */
	void analyse();


	
	/*
	 *  gets a isntance of the lexicalAnalyser class 
     *
	 * Returns 1 instance of the class  
	 */
	LexicalAnalyser	getInstance();
};

